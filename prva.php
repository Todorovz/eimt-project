<!DOCTYPE html>
<?php

    session_start();
    if(empty($_SESSION['user'])){

        header('location:index.php');

    }
   



    ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Index</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <style type="text/css">
     .btn-pink {
    color: #f8f8f8;
    background-color: #E81D62;
    border-color: #c51954;
}
.btn-pink:hover {
    color: #fff;
    background-color: #c51954;
    border-color: #c51954;
}
.h2, h2 {
    font-size: 30px;
    margin: 30px;
}

input[type=text] {
    width: 130px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-position: 10px 10px; 
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}
.center-block {
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.input-group .icon-addon .form-control {
    border-radius: 0;
}

.icon-addon {
    position: relative;
    color: #555;
    display: block;
}

.icon-addon:after,
.icon-addon:before {
    display: table;
    content: " ";
}

.icon-addon:after {
    clear: both;
}

.icon-addon.addon-md .glyphicon,
.icon-addon .glyphicon, 
.icon-addon.addon-md .fa,
.icon-addon .fa {
    position: absolute;
    z-index: 2;
    left: 10px;
    font-size: 14px;
    width: 20px;
    margin-left: -2.5px;
    text-align: center;
    padding: 10px 0;
    top: 1px
}

.icon-addon.addon-lg .form-control {
    line-height: 1.33;
    height: 46px;
    font-size: 18px;
    padding: 10px 16px 10px 40px;
}

.icon-addon.addon-sm .form-control {
    height: 30px;
    padding: 5px 10px 5px 28px;
    font-size: 12px;
    line-height: 1.5;
}

.icon-addon.addon-lg .fa,
.icon-addon.addon-lg .glyphicon {
    font-size: 18px;
    margin-left: 0;
    left: 11px;
    top: 4px;
}

.icon-addon.addon-md .form-control,
.icon-addon .form-control {
    padding-left: 30px;
    float: left;
    font-weight: normal;
}

.icon-addon.addon-sm .fa,
.icon-addon.addon-sm .glyphicon {
    margin-left: 0;
    font-size: 12px;
    left: 5px;
    top: -1px
}

.icon-addon .form-control:focus + .glyphicon,
.icon-addon:hover .glyphicon,
.icon-addon .form-control:focus + .fa,
.icon-addon:hover .fa {
    color: #2580db;
}


     html {
         position: relative;
         min-height: 100%;
     }
     body {
         /* Margin bottom by footer height */
         margin-bottom: 700px;
         background-color: #e2235f;
     }
     .footer {
         position: absolute;
         bottom: 0;
         width: 100%;
         /* Set the fixed height of the footer here */
        height: 700px;
         background-color: #262626;
     }

     @media only screen and (max-width:991px){
         .footer{
             height:160px;
         }
         body{
             margin-bottom:150px;
         }
         #content1{
             margin-bottom:170px !important;
         }
     }
     @media only screen and (min-width:992px) and (max-width:1002px){
         .footer{
             height:720px;
         }
         body{
             margin-bottom:720px;
         }
         #content1{
             margin-bottom:740px !important;
         }
     }
     @media only screen and (min-width:1279px){
         #slika1{
             padding-right:20px;
         }
         #slika2{
             padding-right:20px;
         }
     }

     @media only screen and (min-width:1027px) and (max-width:1090px){
         .footer{
             max-height:700px;
         }
         body{
             margin-bottom:700px;
         }
         #content1{
             margin-bottom:720px !important;
         }
     }

     @media only screen and (min-width:1091px){
         .footer{
             max-height:650px;
         }
         body{
             margin-bottom:650px;
         }
         #content1{
             margin-bottom:670px !important;
         }
     }


     /* Custom page CSS
     -------------------------------------------------- */
     /* Not required for template or sticky footer method. */

     .container {
         width: auto;
         max-width: 680px;
         padding: 0 15px;
     }
     .container .text-muted {
         margin: 20px 0;
     }

     @media only screen and (max-width: 767px) {
         .singleItem{
             min-height: 650px;
         }


     }
     @media only screen and (min-width: 768px) and (max-width: 991px){
        .singleItem{
            max-height: 400px;
        }
     }

     @media only screen and (max-width: 550px) {
         #xsHide{
             margin-right:0px !important;
         }


     }

     @media only screen and (max-width: 399px) {

         .singleItem{
             min-height:700px;
         }

     }

     @media only screen and (min-width:600px) and (max-width: 767px) {

         .singleItem{
             min-height:650px;
         }

     }
     @media only screen and (min-width:320px) and (max-width: 399px) {

         .singleItem {
             min-height: 700px;
         }

     }


     @media only screen and (min-width:768px) and (max-width: 991px) {
         .singleItem{
             max-height:400px;
         }
         .poramni{
             min-height: 350px;
         }
         .divContent{
             position:absolute;
             bottom:70px;
         }


     }

     @media only screen and (min-width:992px) and (max-width: 1240px){
         .singleItem{
             min-height:600px;
         }
         .poramni{
             min-height: 550px;
         }
         .divContent{
             position:absolute;
             bottom:30px;
         }
         #test100{
             margin-right:20px;
         }

     }


     @media only screen and (min-width:1241px){

      .poramni{
          min-height: 450px;
      }

      .divContent{
          position:absolute;
          bottom:60px;
      }
     }
      .navbar{
          margin-bottom:0px!important;
      }

      #footerTop > li a{
          display:block;
          float:left;
          margin-right:70px;
          color:#fff;

      }
    .foot
      .item{
        display:table-cell;
        width:12.5%;
    }
      .foot a{
          color:#fff;
      }

      *{
          margin:0;
          padding:0;
      }
      @media only screen and (min-width:1200px){
          .container{
              min-width:1024px;
          }
      }
     @media only screen and (max-width:1199px){
         .container{
             min-width:960px;
         }
         #kp{
             margin-right: 20px;
         }
     }
     .uredi1{
         margin-top:35px;
     }
     .uredi2{
         margin-top:20px;
     }
      .uredi1 >li {
           padding-bottom:20px;
           padding-right: 10px;
           font-size:12px;
           list-style: none;
           color:#fff;

       }
     .uredi2 >li {
         padding-bottom:20px;
         padding-right: 10px;
         font-size:12px;
         list-style: none;
         color:#fff;

     }
    .item > a h5{
        font-weight:700;
    }

  </style>
  <body>
  <nav class="navbar navbar-inverse">
      <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="margin-top:30px;">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a href="prva.php"><img src="img/telekom-logo3.png"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                  <li style="padding-top:30px; padding-left:30px; color:#e2235f;font-weight: 600; font-size:18px;"><a href="prva.php">Home</a></li>
                  <li style="padding-top:30px; padding-left:30px; color:#e2235f;font-weight: 600; font-size:18px;"><a href="logout.php">Logout</a></li>
              </ul>

          </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
  </nav>



        <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12" style="padding-left:0;padding-right:0; padding-top:20px;">

             <div class="col-md-6 col-sm-6 col-xs-6" style="height:auto;">
                    <input id="searchFilter" style="width:100%;height:50px;" type="text" placeholder="Search.." class="form-control" onkeyup="prebaraj();">
                    <label class="glyphicon glyphicon-search" style="top: -35px; left:10px;" rel="tooltip" title="email"></label>
            </div>

             <div class="col-md-6 col-sm-6 col-xs-6">
                    <select id="availableFilter" class="form-control pull-right" style="width:100%;height:50px; ">
                        <option value="0">ALL</option>
                        <option value="available">Available</option>
                        <option value="unavailable">Unavailable</option>

                    </select>
            </div>

        </div>

    <div id="content1" class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-bottom: 720px;"></div>


  <footer class="footer">
      <div class="hidden-sm hidden-xs">
      <div class="container-fluid">
          <div class="col-lg-12 col-md-12" style="margin-top:20px; margin-bottom:30px;">
                  <ul id="footerTop" style="list-style-type:none; color:#fff;">
                      <li><a href="#">Приватни</a></li>
                      <li><a href="#">Деловни</a></li>
                      <li><a href="#">За компанијата</a></li>
                      <li><a href="#">Контакт</a></li>
                  </ul>
          </div>
          <div class="col-lg-12 col-md-12 foot" style="display: table;">


              <div class="item"><a href="#"><h5>МАГЕНТА 1</h5></a></div>
              <div class="item"><a href="#"><h5>УРЕДИ</h5></a>
                <ul class="uredi1">
                    <li>Купи онлајн</li>
                    <li>Останати категории</li>
                </ul>
              </div>
              <div class="item"><a href="#"><h5>МОБИЛНИ УСЛУГИ</h5></a>
                  <ul class="uredi2">
                      <li>Постпејд</li>
                      <li>Притпејд</li>
                  </ul>
              </div>
              <div class="item"><a href="#"><h5>ТЕЛЕВИЗИЈА</h5></a>
                  <ul class="uredi1">
                      <li>Пакети</li>
                      <li>Дополнителни ТВ пакети</li>
                      <li>ТВ канали</li>
                      <li>MaxTV GO</li>
                      <li>Што е MaxTV</li>
                  </ul>
              </div>
              <div class="item"><a href="#"><h5>ИНТЕРНЕТ</h5></a>
                <ul class="uredi1">
                    <li>Пакети</li>
                    <li>Дополнително</li>
                </ul>
              </div>
              <div class="item"><a href="#"><h5>ОСТАНАТИ УСЛУГИ</h5></a>
                  <ul class="uredi2">
                      <li>Фиксни услуги</li>
                      <li>Мобилни апликации</li>
                      <li>Loop</li>
                      <li>MobiPay</li>
                      <li>MobiPay наградна игра</li>
                      <li>Сите останати услуги</li>
                  </ul>
              </div>
              <div class="item" id="kp"><a href="#"><h5>КОРИСНИЧКА ПОДДРШКА</h5></a>
                  <ul class="uredi2">
                      <li>Телеком продавници</li>
                      <li>FAQ</li>
                      <li>Упатства</li>
                      <li>Моја сметка</li>
                      <li>Мрежно покривање</li>
                      <li>Ценовници, услуги и услови за користење</li>
                      <li>Онлајн корисничка поддршка</li>
                      <li>Сурфајте безбедно</li>
                  </ul>
              </div>
              <div class="item"><a href="#"><h5>МОЈ КЛУБ</h5></a>
                  <ul class="uredi1">
                      <li>Корисници на мобилни услуги</li>
                      <li>Корисници на фиксни услуги</li>
                      <li>Општи услови и информации</li>
                  </ul>
              </div>
              </div>
      </div>

          <div class="container-fluid"><hr style="border-top:1px solid black;"></div>
      </div>
          <div class="container-fluid" style="margin-top:20px;">
              <div class="row">
                  <div class="col-sm-6 col-xs-4 col-md-6">
                      <img src="img/telekom-logo1edited.png">
                  </div>
                  <div id="test100" class="col-md-2 pull-right">
                      <img src="img/spodelidozivuvanja.png" class="hidden-sm hidden-xs">
                      <div class="col-lg-2 col-md-2 hidden-md hidden-lg pull-right">
                          <img id="slika1" src="img/facebook2.png" style="padding-right:5px;">
                          <img id="slika2" src="img/twitter2.png" style="padding-right:5px;">
                          <img src="img/youtube2.png">
                      </div>
                  </div>
              </div>
              <hr class="hidden-md hidden-lg" style="border-top:1px solid black;">
              <div class="row" style="padding-top:20px;  margin-right:10px;">
                  <div class="col-md-5 col-lg-5 " style="color:darkgray;">
                      ©2017 Македонски Телеком | Правни напомени
                  </div>
                  <div class="col-lg-2 col-md-2 hidden-sm hidden-xs pull-right">
                      <img id="slika1" src="img/facebook2.png">
                      <img id="slika2" src="img/twitter2.png">
                      <img src="img/youtube2.png">
                  </div>
              </div>
          </div>
      </div>
  </footer>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="script.js"></script>
  </body>
</html>