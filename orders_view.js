$(document).ready(function(){
    orderList();
    phoneList();

});


function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function orderList() {
    ordID = getParameterByName('id');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/orders/'+ordID,
        data: $('#cityDetails').serialize(),
        dataType:"json", //to parse string into JSON object,
        success: function(data){
            $('#orderID').val(data.id);
            $('#name').val(data.name);
            $('#address').val(data.address);
            $('#email').val(data.email);
            $('#phone').val(data.phone);
            $('#comment').val(data.comment);
            $('#phoneID').val(data.phoneID);
            $('#package').val(data.package);
            $('#status').val(data.status);

        }
    });


}

function phoneList() {
    phID = getParameterByName('PID');
    console.log(phID);
    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/phones_data/'+phID,
        dataType:"json", //to parse string into JSON object,
        success: function(data){
            $('#phoneViewPanel').css('background', 'url('+data.img_src+')').css('background-repeat','no-repeat').css('background-size', 'contain').css('height','250px');
            $('#phoneName').val(data.product_name);
            $('#phonePrice').val(data.price);
            $('#phonePP').val(data.package_price);
            $('#available').val(data.available);

        }
    });


}


