<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
<style>
form {
    border: 3px solid #f1f1f1;

}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 20%;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}


span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}

    #formSignIn{
        border:0 !important;

    }
html {
    position: relative;
    min-height: 100%;
}
body {
    /* Margin bottom by footer height */
    margin-bottom: 100px;
    background-color: #e2235f;
}
.footer {
    position: absolute;
    bottom: 0;
    width: 100%;
    /* Set the fixed height of the footer here */
    height: 100px;
    background-color: #262626;
}


/* Custom page CSS
-------------------------------------------------- */
/* Not required for template or sticky footer method. */

.container {
    width: auto;
    max-width: 680px;
    padding: 0 15px;
}
.container .text-muted {
    margin: 20px 0;
}
@media only screen and (min-width:320px) and (max-width:991px){
    #formSignIn{
        padding-top:50px;
    }
}

</style>
</head>
<body>

    <div class="col-md-12 col-sm-12 col-xs-12 " style="background-color:#333; height:100px; min-width: 100%;">

            <div class="col-md-1"></div>
    <div class="col-md-3 img-responsive" id="logo" style="background:url('img/telekom-logo.png')no-repeat; background-size:contain; height:99px; "></div>
            <div class="col-md-8"></div>
    </div>
<div class="col-md-12" id="content">
<div class=col-md-4></div>
<div class=col-md-4 style="padding-top:100px;">
<form class="form-signin" id="formSignIn">
    <h1 class="form-signin-heading" style="text-align:center; color:#fff;">НАЈАВЕТЕ СЕ </h1>
    <p class="form-signin-paragraph" style="text-align:center;color:#fff">Мој Телеком - Сè на едно место!</p>
    <label for="inputUser" class="sr-only">Email address</label>
    <input type="text" id="inputUser" class="form-control" placeholder="Вашето корисничко име" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Вашата лозинка" required>

    <button class="btn btn-lg btn-primary btn-block" id="login" type="submit">Најави ме</button>

  </form>
</div>
<div class=col-md-4></div>
</div>
</div>
<footer class="footer">
    <div class="container-fluid">
        <div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9 col-xs-6 col-xs-offset-6" style="background:url('img/logo.png') no-repeat; background-size:contain; height:100px;"></div>
    </div>
</footer>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="login.js" type="text/javascript"></script>
</body>
</html>


