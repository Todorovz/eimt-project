$(document).ready(function(){
loadItem();


});
   

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function loadItem() {
        id = getParameterByName('id');
        $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/phones_data/'+id,
        data: $('#cityDetails').serialize(),
        dataType:"json", //to parse string into JSON object,
        success: function(data){ 
          $('#img').css('background', 'url('+data.img_src+')').css('background-repeat','no-repeat').css('background-size', 'contain').css('height','500px');
          $('#user_id').val(data.id);
          $('#user_name').val(data.product_name);
          $('#manufacturer').val(data.manufacturer);
          $('#price').val(data.price);
          $('#packagePrice').val(data.package_price);
          $('#package').val(data.package);
          $('#available').val(data.available);
          $('#internet').val(data.internet);
          $('#screen').val(data.screen);
          $('#camera').val(data.camera);
          $('#os').val(data.OS);
          $('#cpu').val(data.CPU);
          $('#memory').val(data.memory);
          $('#fourG').val(data.fourG);
          $('#threeG').val(data.threeG);


        }
    });


    }
