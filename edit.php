<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
    .list-group-item{
        border:0;
    }
</style>
</head>
<body>
<div class="col-md-12">
    <p class="user pull-right" style="padding-top:20px; padding-left: 20px;"><b>
            <?php

            session_start();
            if(empty($_SESSION['user'])){

                header('location:index.php');

            }
            else{
                echo  "Добредојде,  " . $_SESSION['user'];

            }

            ?>
        </b></p>
</div>
<div class="col-md-12">
    <button onclick="window.location='logout.php'" class="btn pull-right" style="margin-right:20px; margin-bottom:10px; margin-top:10px;">Log out</button>
    <button onclick="window.location='admin.php'" class="btn" style="margin-left:20px; margin-bottom:10px; margin-top:10px;">Go back</button>
</div>

<h2 style="text-align: center; padding-bottom: 30px;">Admin edit Product panel</h2>

    <div id="adminViewDiv" class="col-md-12">
    <div class="col-md-2"></div>
    <form id="editForm" class="col-md-7">
    <ul>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%">ID</div><div class="col-md-8" style="height:100%"><input name="id" style="height:100%" class="form-control" id="user_id" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%">Image Source</div><div class="col-md-8" style="height:100%"><input name="img_src" style="height:100%" class="form-control" id="imgUrl" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%">Name</div><div class="col-md-8" style="height:100%"><input name="product_name" style="height:100%" class="form-control" id="user_name" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Manufacturer</div><div class="col-md-8" style="height:100%"><input name="manufacturer" style="height:100%" class="form-control" id="manufacturer" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Price</div><div class="col-md-8" style="height:100%"><input name="price" style="height:100%" class="form-control" id="price" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Package Price</div><div class="col-md-8" style="height:100%"><input name="package_price" style="height:100%" class="form-control" id="packagePrice" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Package</div><div class="col-md-8" style="height:100%"><select id="package" name="package" style="height: 100%" class="selectpicker form-control">
            <option value="12 доверба">12 доверба</option>
            <option value="24 доверба">24 доверба</option>
            <option value="48 доверба">48 доверба</option>
        </select>
        </div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Available</div><div class="col-md-8" style="height:100%"><select id="available" name="available" style="height: 100%" class="selectpicker form-control">
            <option value="yes">Yes</option>
            <option value="no">No</option>
        </select></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Internet</div><div class="col-md-8" style="height:100%"><input name="internet" style="height:100%" class="form-control" id="internet" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Screen</div><div class="col-md-8" style="height:100%"><input name="screen" style="height:100%" class="form-control" id="screen" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Camera</div><div class="col-md-8" style="height:100%"><input name="camera" style="height:100%" class="form-control" id="camera" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">OS</div><div class="col-md-8" style="height:100%"><input name="OS" style="height:100%" class="form-control" id="os" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">CPU</div><div class="col-md-8" style="height:100%"><input name="CPU" style="height:100%" class="form-control" id="cpu" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">Memory</div><div class="col-md-8" style="height:100%"><select id="memory" name="memory" style="height: 100%" class="selectpicker form-control">
            <option value="16GB">16GB</option>
            <option value="32GB">32GB</option>
            <option value="64G">64GB</option>
            <option value="128GB">128GB</option>
            <option value="256GB">256GB</option>
        </select></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">4G</div><div class="col-md-8" style="height:100%"><input name="fourG" style="height:100%" class="form-control" id="fourG" /></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-2" style="padding-top:7px; font-size:18px;height:100%;">3G</div><div class="col-md-8" style="height:100%"><input name="threeG" style="height:100%" class="form-control" id="threeG" /></div></li>

    </ul>
    </form>
    <div id="img" class="col-md-3"></div>
</div>
<div style="text-align: center;margin:20px;">
    <button class="btn btn-primary btn-lg" onclick="promeni();">Submit</button>
</div>


 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="edit.js"></script>
</body>
</html>


