<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Index</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .col-md-8{
            padding-right:0px !important;
        }
    </style>
</head>

<body>
<div class="col-md-12">
    <p class="user pull-right" style="padding-top:20px; padding-left: 20px;"><b>
            <?php

            session_start();
            if(empty($_SESSION['user'])){

                header('location:index.php');

            }
            else{
                echo  "Добредојде,  " . $_SESSION['user'];

            }

            ?>
        </b></p>
</div>
<div class="col-md-12">
    <button onclick="window.location='logout.php'" class="btn pull-right" style="margin-right:20px; margin-bottom:10px; margin-top:10px;">Log out</button>
    <button onclick="window.location='orders.php'" class="btn" style="margin-left:20px; margin-bottom:10px; margin-top:10px;">Go back</button>
</div>
<h2 style="text-align:center; padding-bottom:30px;"> Order View </h2>
<div class="col-md-12">
    <div id="orderViewPanel" class="col-md-6">
        <ul>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%">ID</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="orderID" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%">Name</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="name" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%">Address</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" name="address" id="address" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">E-mail</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="email" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Phone</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="phone" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Comment</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="comment" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">PhoneID</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="phoneID" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Package</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="package" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Status</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="status" disabled/></div></li>


        </ul>
    </div>
    <div class="col-md-6">
        <div id="phoneViewPanel" style="margin-bottom: 30px;"></div>
        <ul style="margin-top: 100px;">
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%">Phone name:</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="phoneName" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%">Price:</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="phonePrice" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Package price:</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="phonePP" disabled/></div></li>
            <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Available:</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="available" disabled/></div></li>
        </ul>


    </div>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="orders_view.js"></script>
</body>
</html>