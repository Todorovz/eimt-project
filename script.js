$(document).ready(function(){


    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/phones_data',
        data: $('#cityDetails').serialize(),
        dataType:"json", //to parse string into JSON object,
        success: function(data){ 
            if(data){
                var len = data.length;
                var txt = "";
                if(len > 0){
                    for(var i=0;i<len;i++){
                        if(data[i].id && data[i].product_name){
                            
                            if (data[i].available == "yes") {
                            txt += '<div class="col-md-4 singleItem available" id="data'+data[i].id+'" style="background:#fff; height:500px; padding:15px; border:5px inset #E2235F; border-radius:20px;"><br><div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-6 col-sm-6 col-xs-12" style="background:url('+data[i].img_src+') no-repeat; height:300px; background-size:contain; padding-right:0;"></div><div class="col-md-6 col-sm-6 col-xs-12 poramni" ><h3>' +data[i].product_name+ '</h3><div class="divContent"><h4>Производител :  '+data[i].manufacturer+'</h4><h4>Цена :  '+data[i].price+' ден.</h4><h4>Цена на пакет :  '+data[i].package_price+' ден./месец</h4><h4>Пакет : ' +data[i].package +'</h4><div><h4>Достапност: <span class="glyphicon glyphicon-ok " style="font-size: 18px; padding-left:15px; color:green;"></span></h4></div><div class="btnDetails"><button onclick="window.location.href=\'phone_details.php?id=' + data[i].id + '\'" class="btn btn-pink" type="button">Details</button></div></div></div></div></div>';
                                      }
                              else{
                            txt += '<div class="col-md-4 singleItem unavailable" style="background:#fff; height:500px; padding:15px; border:5px inset #E2235F; border-radius:20px;"><br><div class="col-md-12 col-sm-12 col-xs-12"><div class="col-md-6 col-sm-6 col-xs-12" style="background:url('+data[i].img_src+') no-repeat; height:300px; background-size:contain; "></div><div class="col-md-6 col-sm-6 col-xs-12 poramni" ><h3>' +data[i].product_name+ '</h3><div class="divContent"><h4>Производител :  '+data[i].manufacturer+'</h4><h4>Цена :  '+data[i].price+' ден.</h4><h4>Цена на пакет :  '+data[i].package_price+' ден./месец</h4><h4>Пакет : ' +data[i].package +'</h4><div><h4>Достапност: <span class="glyphicon glyphicon-remove " style="font-size: 18px; padding-left:15px; color:red;"></span></h4></div><div class="btnDetails"><button onclick="window.location.href=\'phone_details.php?id=' + data[i].id + '\'" class="btn btn-pink" type="button">Details</button></div></div></div></div></div>';

                
                              }



                        }
                       
                    }
                    if(txt != ""){
                        $("#content1").append(txt);
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus + ': ' + errorThrown);
        }
    });
    return false;//suppress natural form submission
});



function prebaraj(){
    var valSearch = jQuery('#searchFilter').val().toLowerCase();
    
    if(valSearch == ""){
        jQuery('.singleItem').show();
    }
    else{
        jQuery('.singleItem').each(function()
            {
            var text = jQuery(this).text().toLowerCase();
             (text.indexOf(valSearch) >= 0) ? jQuery(this).show() : jQuery(this).hide();
        });
    }

}
jQuery('#availableFilter').on('change',function(){
    var e = document.getElementById("availableFilter");
    var strUser = e.options[e.selectedIndex].value;
    if(strUser == "available"){
        jQuery('.unavailable').addClass("hidden");
        jQuery('.available').removeClass("hidden");
    }
    else if(strUser == "unavailable"){
        jQuery('.available').addClass("hidden");
        jQuery('.unavailable').removeClass("hidden");
    }
    else{
        jQuery('.available').removeClass("hidden");
        jQuery('.unavailable').removeClass("hidden");
    }

});