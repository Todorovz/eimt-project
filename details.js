$(document).ready(function(){
    loadItem();
    payment();

    $('#prev').on('click', function(){
        $('#step2').addClass('hidden');
        $('#step1').removeClass('hidden');
    });




});



$('#next').click(function () {
    var name= $('#name1').val();
    var email= $('#email1').val();
    var address= $('#address1').val();
    var phone= $('#phone1').val();

    if($('#name1').val() != '' && $('#email1').val() != '' && $('#address1').val() != '' && $('#phone1').val() != ''){
        $('#step2').removeClass('hidden');
        $('#step1').addClass('hidden');
    }
    else if($('#name1').val() =='' || $('#email1').val() =='' || $('#address1').val() =='' || $('#phone1').val() == ''){
        alert('Fill the empty fields');
    }
});






function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function loadItem() {
        myvar = getParameterByName('id');


        $.getJSON("http://localhost:3000/phones_data/"+myvar, function (obj)



        {
            $('#price').val(obj.price);
            $('#picture').val(obj.img_src);
            $('#phoneID').val(obj.id);
            $('#package').val(obj.package);
            $('#phonePrice').val(obj.price);




            $(".itemName").append('<h2 class="nameLeft">' +obj.product_name+ '</h2>');
            $(".itemNameModal").append('<h4 class="nameLeft">' +obj.product_name+ '</h4>');
            $(".itemManufacturer").append('<h2 class="nameLeft">' +obj.manufacturer+ '</h2>');
            $(".itemPrice").append('<h2 class="nameLeft">' +obj.price+ ' ден.</h2>');
            $("<div class='itemImage' style='background:url(" + obj.img_src + ") no-repeat;background-size:contain;height:500px;'> </div>").appendTo("#slika");
            $(".itemPackagePrice").append('<h2 class="nameLeft">' + obj.package_price +' ден.</h2>');
            $(".itemPackage").append('<h2 class="nameLeft">' + obj.package +'</h2>');
            $(".itemInternet").append('<h2 class="nameLeft">' + obj.internet +' GB</h2>');
            $(".itemScreen").append('<h2 class="nameLeft">' + obj.screen +'</h2>');
            $(".itemCamera").append('<h2 class="nameLeft">' + obj.camera +'</h2>');
            $(".itemOS").append('<h2 class="nameLeft">' + obj.OS +'</h2>');
            $(".itemCPU").append('<h2 class="nameLeft">' + obj.CPU +'</h2>');
            $(".itemMemory").append('<h2 class="nameLeft">' + obj.memory +'</h2>');
            $(".item4G").append('<h2 class="nameLeft">' + obj.fourG +'</h2>');
            $(".item3G").append('<h2 class="nameLeft">' + obj.threeG +'</h2>');
            $(".cenaDole").append('<h2 class="cenaLeft"> Цена: ' +obj.price+ ' ден.</h2>');


            if (obj.available === "yes") {
                $('.nedostapen').hide();
                }
                else{
                    $('.dostapen').hide();
                
            }

        });

    }


function sentEmail() {
    $.ajax({
        type: 'POST',
        url: 'email1.php',
        data: $("#orderForm").serialize(),
        success: function(data){
            alert('Вашата нарачка беше успешна!');


        }
    });
    return false;
}


function addNewOrder(){
    $.ajax({
        type: 'POST',
        url: 'http://localhost:3000/orders',
        data: jQuery('#orderForm').serialize(),
        dataType:"json", //to parse string into JSON object,
        success: function(data){

            $('#myModal').modal('hide');
        }
    });
    return false;
}

function promeni(id) {

        console.log(id);
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:3000/payment/' + id,
            data: $('#newBalanceForm').serialize(),
            dataType: "json", //to parse string into JSON object,
            success: function (data) {
                alert("Payment balance updated");

            }
        });
        return false;

}


function payment() {

    $('#finalBtn').click(function(){
        var name = $('#cardHolder').val();
        var card = $('#cardNumber').val();
        var expDate = $('#expiredate').val();
        var ccv = $('#ccv').val();
        var phonePrice = $('#price').val();
        var error = true;
        var paymentID;

        $.ajax({
            type: 'GET',
            url: 'http://localhost:3000/payment/',
            dataType: "json", //to parse string into JSON object,
            success: function (data) {


                for(i=0;i<data.length;i++) {
                   var stat = data[i].status;
                    var cash = parseInt(data[i].cashbalance);

                    if (name == data[i].name && card == data[i].cardnumber && expDate == data[i].expiredate && ccv == data[i].ccv) {

                        if(cash >= phonePrice){
                            paymentID = data[i].id;
                            addNewOrder();
                            sentEmail();
                            var newBalance = cash-phonePrice;
                            jQuery('#paymentName').val(name);
                            jQuery('#cardNumer').val(card);
                            jQuery('#paymentCCV').val(ccv);
                            jQuery('#paymentExpireDate').val(expDate);
                            jQuery('#paymentStatus').val(stat);
                            jQuery('#paymentBalance').val(newBalance);
                            promeni(paymentID);

                        }
                        else{
                            alert("The payment can't be processed, please check your balance.");
                        }
                        break;

                    }
                    else{
                        console.log("Ignore this data.");

                    }

                }


            }
        });
        return false;
    });
    return false;
}



