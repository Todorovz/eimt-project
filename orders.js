/**
 * Created by Todorov on 29-Mar-17.
 */
$(document).ready(function(){

    listAllOrders();

});

function deletePhone(id){
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:3000/phones_data/'+id,
        dataType:"json", //to parse string into JSON object,
        success: function(data){
            alert("deleted");
            var table = jQuery('#example').dataTable();
            table.empty();
            table.fnDestroy();
            listAllPhones();



        }
    });
}
function listAllOrders(){
    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/orders',
        dataType:"json", //to parse string into JSON object,
        success: function(data){
            if(data){
                var len = data.length;
                var txt = "";
                var content1 = '<table id="example" class="display" cellspacing="0" width="100%" style="text-align:center;">'
                    +'<thead>'
                    +    '<tr>'
                    +     ' <th>Name</th>'
                    +     ' <th>Adress</th>'
                    +     ' <th>E-mail</th>'
                    +   ' <th>Phone</th>'
                    +   ' <th>PhoneID</th>'
                    +   ' <th>Package</th>'
                    +   ' <th>Status</th>'
                    +   ' <th style="text-align:right;">Actions</th>'
                    + '</tr>'
                    + '</thead>'
                    + '<tbody>';
                var content2 = ' </tbody></table>';

                if(len > 0){
                    for(var i=0;i<len;i++){


                            txt += '<tr>'
                                + '<td>'+data[i].name+'</td>'
                                + '<td>'+data[i].address+'</td>'
                                + '<td>'+data[i].email+'.</td>'
                                + '<td>'+data[i].phone+'</td>'
                                + '<td>'+data[i].phoneID+'</td>'
                                + '<td>'+data[i].package+'</td>'
                                + '<td>'+data[i].status+'</td>'
                                + '<td ><button id="btn" class="btn btn-primary" onclick="window.location.href=\'../sampleSiteT/orders_view.php?id='+data[i].id+'&&PID='+data[i].phoneID+'\'">More details</button><button style="margin:5px;" id="btn1" class="btn btn-primary" onclick="window.location.href=\'../sampleSiteT/orders_edit.php?id='+data[i].id+'&&PID='+data[i].phoneID+'\'">Edit</button></td>'
                                + '</tr>';

                    }



                    if(txt != ""){
                        $("#contentID").html(content1 + txt + content2);
                        $('#example').DataTable( {
                            responsive: true
                        } );
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus + ': ' + errorThrown);
        }
    });
    return false;//suppress natural form submission
}



function addNewPhone(){
    $('#addForm').submit(function(){
        $.ajax({
            type: 'POST',
            url: 'http://localhost:3000/phones_data',
            data: jQuery('#addForm').serialize(),
            dataType:"json", //to parse string into JSON object,
            success: function(data){
                var table = jQuery('#example').dataTable();
                table.empty();
                table.fnDestroy();
                listAllPhones();
                $('#myModal').modal('hide');
            }
        });
        return false;
    });
}
function filteringAvailability(){
    jQuery('#availableFilter').on('change',function(){
        var t1 = jQuery('#availableFilter').val();
        if(t1 == "available"){
            jQuery('.available').removeClass('hide');
            jQuery('.unavailable').addClass('hide');

        }
        else if(t1 == "unavailable"){
            jQuery('.unavailable').removeClass('hide');
            jQuery('.available').addClass('hide');
        }
        else if(t1=="0"){
            jQuery('.available').removeClass('hide');
            jQuery('.unavailable').removeClass('hide');
        }
    });
}



