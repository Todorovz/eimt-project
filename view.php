<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Index</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      <style>
          .col-md-8{
              padding-right:0! important;
          }
      </style>
  </head>

  <body>
  <div class="col-md-12">
      <p class="user pull-right" style="padding-top:20px; padding-left: 20px;"><b>
              <?php

              session_start();
              if(empty($_SESSION['user'])){

                  header('location:index.php');

              }
              else{
                  echo  "Добредојде,  " . $_SESSION['user'];

              }

              ?>
          </b></p>
  </div>
  <div class="col-md-12">
      <button onclick="window.location='logout.php'" class="btn pull-right" style="margin-right:20px; margin-bottom:10px; margin-top:10px;">Log out</button>
      <button onclick="window.location='admin.php'" class="btn" style="margin-left:20px; margin-bottom:10px; margin-top:10px;">Go back</button>
  </div>
    <h2 style="text-align:center; padding-bottom:30px;"> Details VIEW </h2>
    <div class="col-md-12">
    <div id="adminViewDiv" class="col-md-8">
    <ul>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%">ID</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="user_id" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%">Name</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="user_name" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Manufacturer</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="manufacturer" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Price</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="price" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Package Price</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="packagePrice" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Package</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="package" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Available</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="available" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Internet</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="internet" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Screen</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="screen" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Camera</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="camera" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">OS</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="os" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">CPU</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="cpu" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">Memory</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="memory" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">4G</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="fourG" disabled/></div></li>
        <li class="list-group-item" style="padding:0;height: 50px;margin-bottom:20px;"><div class="col-md-4" style="padding-top:7px; font-size:18px;height:100%;">3G</div><div class="col-md-8" style="height:100%"><input style="height:100%" class="form-control" id="threeG" disabled/></div></li>

    </ul>
</div>
<div class="col-md-4" id="img" style=""></div>
</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="view.js"></script>
  </body>
</html>